#!/usr/bin/env bash


echo '#########################################################'
	

echo -e '\e[1;34mSCRIPT PARA INFORMACAO DO SISTEMA\e[0m'

echo '########################################################'


echo -e '\e[0;94mUSUARIO:\e[0m'
whoami
echo '---------------------------------------------------------'

echo -e '\e[0;94mNOME DO COMPUTADOR:\e[0m '
hostname
echo '---------------------------------------------------------'

echo -e '\e[0;94mENDEREÇO IP INTERNO:\e[0m'
hostname -I
echo '---------------------------------------------------------'

echo -e '\e[0;94mENDEREÇO IP EXTERNO:\e[0m'
wget -qO- http://ipecho.net/plain
echo ''
echo '---------------------------------------------------------'
echo -e '\e[0;94mTEMPO DE FUNCIONAMENTO:\e[0m'
uptime -p
echo '---------------------------------------------------------'

echo -e '\e[0;94mSISTEMA:\e[0m '
uname -rms
cat /etc/*-release |grep ID
echo '--------------------------------------------------------'

echo -e '\e[0;94mINTERPRETADOR SHELL:\e[0m'
echo $SHELL
echo '--------------------------------------------------------'
echo -e '###################################################'
#echo -e 'por \e[5mflira\e[0'
#echo -e '##################################################'

exit
---
