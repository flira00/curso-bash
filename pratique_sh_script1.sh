#!/usr/bin/env bash


echo '#########################################################'


echo -e '/ ___| / ___|  _ \|_ _|  _ \_   _| |  _ \ / \  |  _ \    / \   
\___ \| |   | |_) || || |_) || |   | |_) / _ \ | |_) |  / _ \  
 ___) | |___|  _ < | ||  __/ | |   |  __/ ___ \|  _ <  / ___ \ 
|____/ \____|_| \_\___|_|    |_|   |_| /_/   \_\_| \_\/_/   \_\
                                                               
 ____  ____      _  _____ ___ ___  _   _ _____ 
|  _ \|  _ \    / \|_   _|_ _/ _ \| | | | ____|
| |_) | |_) |  / _ \ | |  | | | | | | | |  _|  
|  __/|  _ <  / ___ \| |  | | |_| | |_| | |___ 
|_|   |_| \_\/_/   \_\_| |___\__\_\\___/|_____|
'


echo '##########################################################'
echo -e 'Informações básicas do sistema'
uname -a
echo ''
echo '##########################################################'
echo 'Nome do computador'
hostname
echo ''
echo '##########################################################'
echo -e 'Tempo de funcionamento'
uptime
echo ''
echo '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
# Listar interfaces de rede e endereços IP;
listar_interfaces_ip() {
  echo "+++++ Interfaces de Rede e Endereços IP ++++++"
  ip address | awk '/^[0-9]+:/ { print substr($2, 1, length($2)-1) }' | while read interface; do
    echo "Interface: $interface"
    ip address show $interface | grep -w inet | awk '{print "  " $2}'
  done
  echo ""
}

# Pingar um host ou endereço web
pingar_host() {
  echo -n "Digite o host ou endereço web para verificar conectividade (serão feitos 10 pings): "
  read endereco
  echo ""
  ping -c 10 $endereco
  echo ""
}

# Reiniciar o serviço de rede local
reiniciar_servico_rede_local() {
  echo 'Para reiniciar o serviço digite a senha de administrador.'
  sudo systemctl restart NetworkManager
  echo 'Serviço de rede local reiniciado.'
  echo ''
}

# Setar o IP manualmente em uma interface de rede
setar_ip_manualmente() {
  listar_interfaces_ip
  echo -n "Digite o nome da interface de rede para configurar o IP: "
  read interface
  echo -n "Digite o novo endereço IP para a interface $interface: "
  read ip
  echo -n "Digite a máscara de sub-rede para a interface $interface (/24). Será solicitado a senha de administrador. Após setar o novo ip será necessário reinicar o serviço de rede: "
  read mascara
  sudo ip addr add $ip/$mascara dev $interface
  echo "Endereço IP configurado com sucesso para a interface $interface."
  echo ""
}

menu() {
  echo "------- Escolha o comando desejado -------"
  echo '1. Listar Interfaces de Rede e Endereços IP'
  echo '2. Verifique conectividade com um host externo'
  echo '3. Reinicie seu serviço de rede local em sistemas RHEL'
  echo '4. configure sua interface de rede'
  echo '5. Sair'
  echo ''
  echo -n 'Escolha uma opção: '
}

# menu
while true; do
  menu
  read opcao

  case $opcao in
    1)
      listar_interfaces_ip
      ;;
    2)
      pingar_host
     ;;
    3)
      reiniciar_servico_rede_local
     ;;
    4)
     setar_ip_manualmente
     ;; 
    5)
     echo 'Fim do script'
     break 
      ;;
    *)
      echo "Opção inválida. Por favor, escolha novamente."
      ;;
  esac
done
echo ''
echo '==========================================================================
 _____ ___ __  __   ____   ___  
|  ___|_ _|  \/  | |  _ \ / _ \ 
| |_   | || |\/| | | | | | | | |
|  _|  | || |  | | | |_| | |_| |
|_|   |___|_|  |_| |____/ \___/ 
                                
 ____  ____      _  _____ ___ ___  _   _ _____ 
|  _ \|  _ \    / \|_   _|_ _/ _ \| | | | ____|
| |_) | |_) |  / _ \ | |  | | | | | | | |  _|  
|  __/|  _ <  / ___ \| |  | | |_| | |_| | |___ 
|_|   |_| \_\/_/   \_\_| |___\__\_\\___/|_____|
'
